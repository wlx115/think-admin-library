<?php

// +----------------------------------------------------------------------
// | Library for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海云音信息技术有限公司 [ http://www.yyinfos.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------


declare (strict_types=1);

namespace qhweb;

use think\App;
use think\Service;
/**
 * 模块注册服务
 * @class Service
 * @package qhweb
 */
class Library extends Service
{
    /**
     * 静态应用实例
     * @var App
     */
    public static $sapp;
    /**
     * 静态Request对象
     * @var App
     */
    public static $request;
    /**
     * 启动服务
     * @return void
     */
    public function boot()
    {
        // 静态应用赋值
        static::$sapp = $this->app;
        static::$request = $this->app->request;
        
    }

    /**
     * 初始化服务
     * @return void
     */
    public function register()
    {
        // 动态加载全局配置
        include_once __DIR__.DIRECTORY_SEPARATOR.'common.php';
    }
}