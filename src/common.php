<?php
// +----------------------------------------------------------------------
// | Library for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海云音信息技术有限公司 [ http://www.yyinfos.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------


if (!function_exists('getArrParentIds')) {
    /**
     * 获取数据树父ID集合
     * @param array $list 数据列表
     * @param mixed $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    function getArrParentIds(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $ids = [intval($value)];
        foreach ($list as $vo) if (intval($vo[$ckey]) === intval($value) && intval($vo[$pkey]) > 0) {
            $ids = array_merge($ids, getArrParentIds($list, intval($vo[$pkey]), $ckey, $pkey));
        }
        return in_array(0,$ids) ? $ids : array_merge([0],$ids);
    }
}

if (!function_exists('getArrSubIds')) {
   /**
     * 获取数据树子ID集合
     * @param array $list 数据列表
     * @param mixed $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    function getArrSubIds(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $ids = [intval($value)];
        foreach ($list as $vo) if (intval($vo[$pkey]) > 0 && intval($vo[$pkey]) === intval($value)) {
            $ids = array_merge($ids, getArrSubIds($list, intval($vo[$ckey]), $ckey, $pkey));
        }
        return $ids;
    }
}

if (!function_exists('dealBadwords')) {
    /**
     * 敏感词过滤处理
     * @param $content
     * @return mixed
     * @throws \DfaFilter\Exceptions\PdsBusinessException
     * @throws \DfaFilter\Exceptions\PdsSystemException
     */
    function dealBadwords($content){
        if(!empty($content)){
            $badwordConfig = sysconf('badword.');
            //系统默认的敏感词库
            $badwordFile = trim(app()->getConfigPath(), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'badword.txt';
            $replaceStr = '*';
            if(count($badwordConfig)>0 && $badwordConfig['is_open']){
                //判断自定义文件是否存在
                $configFile = explode('|',$badwordConfig['file']);
                if(count($configFile)>0){
                    $filePath = app()->getRootPath().'safefile'.DIRECTORY_SEPARATOR.$configFile[2];
                    if(file_exists($filePath)){
                        $badwordFile = $filePath;
                        $replaceStr = $badwordConfig['replace'];
                    }
                }
                $wordPool = file_get_contents($badwordFile);
                $wordData = explode(',', $wordPool);
                $handle = \DfaFilter\SensitiveHelper::init()->setTree($wordData);//setTreeByFile()这个方法没有生效
                return $handle->replace($content,$replaceStr);
            }
            return $content;
        }
        return $content;
    }
}