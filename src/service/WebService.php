<?php
// +----------------------------------------------------------------------
// | Library for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海云音信息技术有限公司 [ http://www.yyinfos.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

namespace qhweb\service;

use think\migration\Migrator;
use qhweb\Library;

/**
 * 站点服务
 * Class WebService
 * @package qhweb\service
 */

class WebService
{
    /**
     * 监测站点联通性
     *
     * @param array $domainList
     * @param integer $timeout
     * @return void
     */
    public static function checkDomain($domainList, $timeout = 3)
    {
        set_time_limit(0);
        if(is_array($domainList)){
            $res = [];
            foreach ($domainList as $domian) {
                $logs = self::httpCode($domian,$timeout);
                array_push($res,$logs);
            }
            return $res;
        }else{
            $logs = self::httpCode($domainList,$timeout);
            return $logs;
        }
        return false;
    }

    /**
     *CURL HTTP状态获取
     *
     * @param [type] $url
     * @param integer $timeout
     * @return void
     */
    public static function httpCode($url, $timeout=3)
    {
        $start_time = microtime(true);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_HEADER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_exec($ch);
        $code =  curl_getinfo($ch,CURLINFO_HTTP_CODE);
        curl_close($ch);
        $end_time = microtime(true);
        //记录日志
        $logs = [
            'url'   => $url,
            'code'  => $code,
            'start' => date('Y-m-d H:i:s',$start_time),
            'end'   => date('Y-m-d H:i:s',$end_time),
            'time'  => round($end_time - $start_time,2),
        ];
        self::setLog($url,$logs);
        return $logs;
    }

    private static function setLog($url,$data){ 
        $years = date('Y-m');
        $result = $data['code'] == 200 ? '站点连通性正常' : '站点连通异常';
        //设置路径目录信息
        $logpath = runtime_path().'weblog/'.$years.'/'.$url.'_log.txt';  
        $dir_name=dirname($logpath);
        //目录不存在就创建
        if(!file_exists($dir_name)){
           mkdir(iconv("UTF-8", "GBK", $dir_name),0777,true);
        }
        $log = "==========================================================================================\n";
        $log .= " ★开始检查站点连通性[{$data['start']}]\n";
        $log .= "==========================================================================================\n";
        $log .= "|-监测站点：{$url}\n";
        $log .= "|-监测代码：{$data['code']}\n";
        $log .= "|-监测结果：{$result}\n";
        $log .= "|-站点监测完成，耗时{$data['time']}秒\n";
        $log .= "==========================================================================================\n";
        $log .= "☆检查站点连通性完成[{$data['end']}]\n";
        $log .= "==========================================================================================\n";
        //打开文件资源通道 不存在则自动创建  
        $fp = fopen($logpath,"a");
        //写入文件     
        fwrite($fp,$log."\r\n");
        //关闭资源通道
        fclose($fp);
    }

    /**
     * 获取日志
     *
     * @param [type] $url
     * @return string
     */
    public static function getLog($url)
    {
        $years = date('Y-m');
        $logpath = runtime_path().'weblog/'.$years.'/'.$url.'_log.txt';  
        if(file_exists($logpath)){
            $fp = fopen($logpath,"r");
            $content = fread($fp,filesize($logpath));
            fclose($fp);
            return $content;
        }
        return '';
    }
}