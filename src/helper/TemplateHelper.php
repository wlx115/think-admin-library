<?php
// +----------------------------------------------------------------------
// | LHSystem
// +----------------------------------------------------------------------
// | 版权所有 2014~2020 青海云音信息技术有限公司 [ http://www.yyinfos.com ]
// +----------------------------------------------------------------------
// | 官方网站: https://www.yyinfos.com
// +----------------------------------------------------------------------
// | 作者：独角戏 <qhweb@foxmail.com>
// +----------------------------------------------------------------------
// 
namespace qhweb\helper;

use qhweb\Library;
use think\file\UploadedFile;

/**
 * 模板管理
 * Class TemplateHelper
 * @package qhweb\helper
 */
class TemplateHelper
{
    //模板目录
    protected $tpl_path;
    //当前主题
    protected $theme;
    //历史模板表
    protected $able='CmsTemplate';
    //当前主题目录
    protected $rootpath;
	
    /**
	 * 获取模板列表
	 *
	 * @param string $tplPath  模板路径
	 * @param boolean $isTtop  是否顶级
	 * @return void
	 */
    public static function getTemplateList($tplPath='',$isTop = false)
    {
		$tplArr = [];
		if(empty($tplPath)) return $tplArr;
		$tplArr = self::getFiles($tplPath);
		if($isTop){
			foreach ($tplArr as &$file) {
				if($file['dir']){
					$info = file_exists($file['filepath'].'info.php') ? require $file['filepath'].'info.php' : [];
					$file = array_merge($file, $info);
				}
			}
		}
		return $tplArr;
    }

    /**
     * 列表数据处理
     * @param array $data
     */
    protected static function getFiles($path='')
    {
		$files = glob($path.'/*');
		$sort = $templates = array();
		foreach ($files as $file){
			$filename = basename($file);
			$ext = pathinfo($file,PATHINFO_EXTENSION);
			if($ext == 'php') continue;
			$templates[] = array(
				'filename'  => $filename,
				'filepath'  => $file,
				'dir'       => is_dir($file),
				'filesize'  => format_bytes(filesize($file)),
				'filemtime' => date('Y-m-d H:i:s',filemtime($file)),
				'ext'       => is_dir($file) ? 'folder' : $ext,
				'src'       => str_replace('/','-',$file)
			);
			$sort[] =  is_dir($file);
		}
		//按目录排序
		if ($templates){
			array_multisort($sort,SORT_DESC,$templates);
		}
		return $templates;
    }

    /**
	 * 修改模板 
	 *
	 * @param string $filePath  目录路径
	 * @param string $content  模板内容
	 * @return void
	 */
    public static function saveTemplate($filePath='',$content='')
    {
		$fp= fopen($filePath, "w");  //w是写入模式，文件不存在则创建文件写入。
        $len = fwrite($fp, htmlspecialchars_decode(stripslashes($content)));
        fclose($fp);
		return $len;
    }

    /**
	 * 文件或文件夹重命名
	 *
	 * @param [type] $oldName 旧文件名
	 * @param [type] $newName 新文件名
	 * @return void
	 */
    public static function rename($oldName,$newName)
    {
		//获取当前目录
		$dirname   = dirname($oldName);
		//扫描当前目录下文件
		$files     = self::getFiles($dirname);
		//取出目录下文件名
		$fileNames = array_column($files,'filename');
		//判断新文件名是否已存在
		if(in_array($newName,$fileNames)){
			return false;
		}else{
			$newName = $dirname.DIRECTORY_SEPARATOR.$newName;
			return rename($oldName,$newName);
		}
    }

    /**
     * 删除内容
     * auth true
     * @throws \think\db\exception\DbException
     */
    public static function deleteTemplate($path ='')
    {
		if(is_file($path)){
			@unlink($path);
		}else{
			// 打开指定目录
			if ($handle = @opendir($path)){
				while (($file = readdir($handle)) !== false){
					if (($file !== ".") || ($file !== "..")){
						self::deleteTemplate($path . DIRECTORY_SEPARATOR . $file);
					}
				}
				@closedir($handle);
				rmdir ($path);
			}
		}
    }
    /**
	 * 上传文件
	 *
	 * @param string $tplPath 模板路径
	 * @param string $theme  
	 * @return void
	 */
    public static function upload($savePath='./template')
    {
		$file = Library::$request->file('file');
		if ($file instanceof UploadedFile) {
			$distName = $file->getoriginalName();
			$extension = strtolower($file->getOriginalExtension());
			if ($extension !='html')  {
				return [0,'格式不正确,模板文件只能是HTML'];
			}
			if (!$file->isValid()) {
				return [0,'文件上传异常，文件过大或未上传！'];
			}
			// 上传到本地服务器
			$file->move($savePath, basename($distName));
			return [1,'上传:'.$distName.'成功'];
		} else {
			return [0,'未获取到上传的文件对象！'];
		}
    }
}
